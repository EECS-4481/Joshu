// This is used to ping the site and check for our status
// Taken from https://stackoverflow.com/a/4033310
function httpGet(theUrl) {
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
	xmlHttp.send( null );
	return xmlHttp.responseText;
}

// A sleep function
// Taken from https://stackoverflow.com/a/39914235
function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}
