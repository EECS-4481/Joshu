# Joshu

### Requirements:

+ python 3.7+
+ sqlite3
+ flask
+ flask_bootstrap
+ flask_sqlalchemy
+ flask_login
+ flask_wtf
+ flask_socketio
+ wtforms
+ werkzeug
+ eventlet
+ gunicorn

### Usage:

```
$ mkdir joshu
$ python -m venv joshu/
$ source joshu/bin/activate
$ cd joshu
$ git clone http://gitlab.com/EECS-4481/Joshu.git
$ chmod 777 Joshu
$ cd Joshu
$ chmod 777 joshu.db
$ python joshu.py
```

### Running:

```
$ gunicorn --worker-class eventlet -w 1 app:app
```

### Branch Info:

Originally hosted on Apache, the socket-io POSTs done by users would be considered "Bad Request"s and return a 400.
The project was then redeployed on Nginx and the proxy_pass options were used.
As such, the application was to start as a background process in the server, and the flask-socketio author recommended eventlet.
Eventlet was then used to run the flask application and called by gunicorn.
Nginx still had the issue of 400 in response to POSTs, but a work-around was found by forcing the "Upgrade" header to
requests if it was missing.
From our limited testing, the issue is resolved and thus deployment will be done on Nginx for the alpha release.
For our further releases, we plan to support XAMPP, but that will involve more investigation in addressing the 400 POST errors.
Furthermore, the database will have to be reformatted for MySQL, and the code must support field changes.

### TODO:

```
+ Design the main page and queue page to look better
+ Implement notifications to dashboard when a new message is received
+ Implement feature to end chat session
	+ Both on admin and client side
+ Back button on Sign-Up and Login page
+ Put password confirmation on login page (type again and it must match)
+ Lockout when failed logins exceeds threshold
```
