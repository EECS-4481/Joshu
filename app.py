import os
import time
import uuid

import eventlet
import eventlet.wsgi
from flask import Flask, render_template, redirect, url_for, session, request, jsonify
from flask_bootstrap import Bootstrap
from flask_sqlalchemy  import SQLAlchemy
from werkzeug import secure_filename
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from flask_socketio import SocketIO, send, emit, join_room, leave_room

import joshu.constants as jconstants
from joshu.jforms import MessageForm, LoginForm, RegisterForm

app = Flask(__name__)
app.config['SECRET_KEY'] = 'MySecretKey'
app.config['UPLOAD_FOLDER'] = 'static/uploads'
app.config['SQLALCHEMY_DATABASE_URI'] = jconstants.DATABASE_PATH
bootstrap = Bootstrap(app)
db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
socketio = SocketIO(app)

# A list of available pages on the site
site_map = ["index", "dashboard", "login", "logout", "signup", "queue", "upload"]

###
# DB Functions and types
###

class ChatSession(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    chat_session = db.Column(db.String)
    admin = db.Column(db.String(15))
    user = db.Column(db.String(80))
    atoa = db.Column(db.Boolean())

class AdminQueue(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    admin = db.Column(db.String(15))

class UserQueue(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.String(80))

class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    chat_session = db.Column(db.String)
    sender = db.Column(db.String(80))
    recipient = db.Column(db.String(80))
    content = db.Column(db.String)

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(15), unique=True)
    email = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(80))
    online = db.Column(db.Boolean())

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

###
# Flask
###

@app.route('/', methods=['GET', 'POST'])
def index():
    """Return the generic main page"""
    # Start the process that loops over and checks queues
    #queue_check()
    
    return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    """Create the login page"""
    form = LoginForm()

    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user:
            if check_password_hash(user.password, form.password.data):
                login_user(user, remember=form.remember.data)
                # Remember the user as online
                user.online = True
                db.session.commit()

                return redirect(url_for('dashboard'))

        return '<h1>Invalid username or password</h1>'

    return render_template('login.html', form=form)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    """Create an admin signup/registration form"""
    form = RegisterForm()

    if form.validate_on_submit():
        # Create admin-to-admin chats
        admins = [admin.username for admin in User.query.all()]
        for admin in admins:
            chat_url = str(uuid.uuid4().hex)
            new_chat = ChatSession(chat_session=chat_url,
                                   admin=form.username.data,
                                   user=admin,
                                   atoa=True)
            db.session.add(new_chat)

        # Create new admin
        hashed_password = generate_password_hash(form.password.data, method='sha256')
        new_user = User(username=form.username.data,
                        email=form.email.data,
                        password=hashed_password,
                        online=False)
        db.session.add(new_user)

        db.session.commit()
        
        return redirect(url_for('login'))

    return render_template('signup.html', form=form)

@app.route('/dashboard', methods=['GET', 'POST'])
@login_required
def dashboard():
    """Display the admin dashboard"""
    # Get all online admins
    users = User.query.filter_by(online=True).all()

    # Get all admin chats
    admin_urls = []
    for admin in users:
        # The entry can have our current user in either the "admin" field or "user" field - so get both
        chat_pair = ChatSession.query.filter_by(atoa=True).filter( \
                    ((ChatSession.admin == current_user.username) & (ChatSession.user == admin.username)) | \
                    ((ChatSession.admin == admin.username) & (ChatSession.user == current_user.username))).first()
        if chat_pair:
            chat_url = chat_pair.chat_session
            admin_urls.append((chat_url, admin.username))
    
    return render_template('dashboard.html', admin_urls=admin_urls)


@app.route('/logout')
@login_required
def logout():
    """Logout an admin from the application"""
    # Set their status as offline
    user = User.query.filter_by(username=current_user.username).first()
    user.online = False
    db.session.commit()
    # Remove the admin from the available queue, if queued
    if admin_in_queue(current_user.username):
        AdminQueue.query.filter_by(admin=current_user.username).delete()
        db.session.commit()
    # Logout user and redirect to index page
    logout_user()

    return redirect(url_for('index'))

@app.route('/queue')
def queue():
    """Places the admin or anonymous user into their respective wait queue
       For user, a new ID is generated and they are placed at back of queue"""
    
    if current_user.is_authenticated:
        # If they're logged in and not already in queue, then they're put in the admin queue
        if not admin_in_queue(current_user.username):
            print("Adding " + current_user.username + " to admin queue")
            new_admin = AdminQueue(admin=current_user.username)
            db.session.add(new_admin)
            db.session.commit()
        check_queue()
        return ""
    else:
        # If they have a session ID already, make sure they're not in queue
        if 'anon_id' in session:
            # If their session ID is in queue, remove them from the queue
            if user_in_queue(session['anon_id']):
                UserQueue.query.filter_by(user=session['anon_id']).delete()
                db.session.commit()

        # Generate a new session ID
        session['anon_id'] = str(uuid.uuid4().hex)
        # Place them in the queue
        print("Adding anon to queue")
        new_user = UserQueue(user=session['anon_id'])
        db.session.add(new_user)
        db.session.commit()
        check_queue()
        return render_template("wait.html")

@app.route('/queuedrop')
def queue_drop():
    """Drop the admin or client from their available/wait queue, if they are in queue"""

    # Remove admin from available queue (if they are in queue)
    if current_user.is_authenticated:
        if admin_in_queue(current_user.username):
            AdminQueue.query.filter_by(admin=current_user.username).delete()
            db.session.commit()
    # Remove client from wait queue
    elif 'anon_id' in session:
        if user_in_queue(session['anon_id']):
            UserQueue.query.filter_by(user=session['anon_id']).delete()
            db.session.commit()

    return ""

@app.route('/chatdrop')
def chat_drop():
    """Drop all from the chat the person's in"""
    if current_user.is_authenticated:
        # The only droppable chat from admin perpective is one with a user
        ChatSession.query.filter_by(admin=current_user.username, atoa=False).delete()
    elif 'anon_id' in session:
        ChatSession.query.filter_by(user=session['anon_id']).delete()
    
    db.session.commit()
    return ""

@app.route('/chatsession')
def get_chat_session():
    """Check the URI parameter for the user, return URL of chat session if it exists"""
    if 'anon_id' in session:
        # Return room for anonymous user
        if user_has_chat_session(session['anon_id']):
            chat_url = ChatSession.query.filter_by(user=session['anon_id']).first().chat_session
            return jsonify({'chat_url': chat_url})
    elif current_user.is_authenticated:
        # Check if current_user has an active chat session
        if admin_has_chat_session(current_user.username):
            chat_url = ChatSession.query.filter_by(admin=current_user.username, atoa=False).first().chat_session
            return jsonify({'chat_url': chat_url})
    
    return ""

@app.route('/messages', methods=['GET'])
def get_messages():
    """Return the message history for a chat room"""
    chat_room = request.args.get('room')
    chats = Message.query.filter_by(chat_session=chat_room).all()

    # Convert "chats" to a json format for returning
    chat_list = []
    for chat in chats:
        chat_list.append({"sender": chat.sender,
                          "content": chat.content})
    json_chats = {"chats": chat_list}

    return jsonify(json_chats)

@app.route('/upload', methods=['GET', 'POST'])
def upload():
    """Upload a file to the server"""
    if request.method == "POST":
        f = request.files['file']
        f.save(os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(f.filename)))
        return render_template('upload.html', status="File uploaded successfully")

    return render_template('upload.html')

@app.route('/search', methods=['GET', 'POST'])
def search():
    """Search for a page on the site"""
    if request.method == "POST":
        query_page = request.form.get('q').lower()
        print(query_page)
        if query_page in site_map:
            print("Found")
            return redirect(url_for(query_page))
        else:
            return render_template('index.html', message="Page was not found")
    
    return redirect(url_for('index'))

@app.route('/<chat_room>', methods=['GET'])
def new_chatroom(chat_room):
    """Generate a dynamic URL for the chatroom page and return it to the user"""
    # Prevent users from creating their own chat sessions by checking URI first
    if chat_session_exists(chat_room):
        return render_template("chatroom.html", current_user=current_user)
    
    # Return index page since URI is not an active chat session
    return render_template("index.html")

###
# SocketIO support
###

@socketio.on('join')
def join_chat(data):
    """Add a user to a chat room"""
    username = data['username']
    room = data['room']
    join_room(room)
    print(username + " has entered room: " + room)

@socketio.on('exit')
def exit_chat(data):
    """Exit a user from a chat room"""
    username = data['username']
    room = data['room']
    leave_room(room)
    print(username + " has exited room: " + room)

@socketio.on('message')
def message_event(data):
    """Catch the message a user sends to their chat room and play it back for display"""
    # Add the message to the database
    if data['username'] == "Anonymous":
        recipient = ChatSession.query.filter_by(chat_session=data['room']).first().admin
        sender = ChatSession.query.filter_by(chat_session=data['room']).first().user
    else:
        sender = data['username']
        # We are sending to a user
        if data['recipient'] == "Anonymous":
            recipient = ChatSession.query.filter_by(admin=data['username'], atoa=False).first().user
        else:
            # We are sending to an admin but need to find out their name through the
            recipient = data['recipient']

    new_msg = Message(chat_session=data['room'],
                      sender=sender,
                      recipient=recipient,
                      content=data['message'])
    db.session.add(new_msg)
    db.session.commit()

    emit('response', data, room=data['room'])

###
# Simple DB functions
###

def admin_in_queue(admin):
    return AdminQueue.query.filter_by(admin=admin).first()

def user_in_queue(user):
    return UserQueue.query.filter_by(user=user).first()

def chat_session_exists(chat):
    return ChatSession.query.filter_by(chat_session=chat).first()

def admin_has_chat_session(admin):
    return ChatSession.query.filter_by(admin=admin).first()

def user_has_chat_session(user):
    return ChatSession.query.filter_by(user=user).first()

###
# Backend loop to check queues and create chat sessions
###

def create_chat_session(admin, user):
    """Given an admin and user, create a new chat session and store it"""
    chat_room = uuid.uuid4().hex

    new_chat = ChatSession(chat_session=chat_room, admin=admin, user=user, atoa=False)
    db.session.add(new_chat)
    db.session.commit()

    return new_chatroom(chat_room)

def establish_sessions():
    """Create all chat sessions until one queue is empty"""
    # Pop first admin from queue
    admin = db.session.query(AdminQueue).first().admin
    AdminQueue.query.filter_by(admin=admin).delete()
    # Pop first user from queue
    user = db.session.query(UserQueue).first().user
    UserQueue.query.filter_by(user=user).delete()

    db.session.commit()

    create_chat_session(admin, user)

def check_queue():
    """Loops and checks queues, making chat sessions when needed"""
    user_queue_len = db.session.query(UserQueue).count()
    admin_queue_len = db.session.query(AdminQueue).count()
    if admin_queue_len > 0 and user_queue_len > 0:
        print("Enough users for server to establish chat session(s)...")
        establish_sessions()

if __name__ == '__main__':
    eventlet.wsgi.server(eventlet.listen(('', 5000)), app)
    #socketio.run(app, debug=True)
